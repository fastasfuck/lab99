export const getFullAddress = address => {
    return `${address.line_one}\n${address.line_two || ''}, ${
        address.city
      }, ${address.state}, ${address.zip_code}`
}