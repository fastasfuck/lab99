import { configureStore } from '@reduxjs/toolkit';
import { logout } from 'features/common/slices/auth';

import createReducer from './rootReducer';

const middlewares = [];

const authInterceptor =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    if (action?.payload?.response?.status === 401) {
      dispatch(logout());
    } else {
      next(action);
    }
  };

middlewares.push(authInterceptor);

if (process.env.NODE_ENV === 'development') {
  /* eslint-disable global-require */
  const { createLogger } = require('redux-logger');
  /* eslint-enable global-require */
  const logger = createLogger({
    collapsed: (getState, action, logEntry) => !logEntry.error,
  });

  middlewares.push(logger);
}

const store = configureStore({
  reducer: createReducer(),
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      immutableCheck: false,
      serializableCheck: false,
      thunk: true,
    }).concat(middlewares),
  devTools: process.env.NODE_ENV === 'development',
});

store.asyncReducers = {};

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./rootReducer', () => {
    /* eslint-disable global-require */
    const newRootReducer = require('./rootReducer').default;
    /* eslint-enable global-require */
    store.replaceReducer(newRootReducer.createReducer());
  });
}

export const injectReducer = (key, reducer) => {
  if (store.asyncReducers[key]) {
    return false;
  }
  store.asyncReducers[key] = reducer;
  store.replaceReducer(createReducer(store.asyncReducers));
  return store;
};

export default store;
