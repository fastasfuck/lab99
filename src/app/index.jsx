import ReactDOM from 'react-dom';

import 'styles/app-base.css';
import 'styles/app-components.css';
import 'styles/app-utilities.css';

import App from './application';

const rootReact = document.querySelector('#root');

if (!rootReact) {
  throw new Error("Can't find root element");
}

function render() {
  ReactDOM.render(<App />, rootReact);
}

if (module.hot) {
  module.hot.accept();
}

render();
