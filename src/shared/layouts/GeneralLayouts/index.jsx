import PropTypes from 'prop-types';

function GeneralLayouts({ children }) {
  return <main className="flex flex-1 w-full h-full">{children}</main>;
}

GeneralLayouts.propTypes = {
  children: PropTypes.node.isRequired,
};

export default GeneralLayouts;
