import PropTypes from 'prop-types';
import { forwardRef } from 'react';
import { SnackbarContent } from 'notistack';
import NotificationCard from './NotificationCard';

const NotificationTemplate = forwardRef((props, ref) => {
  const { item } = props;

  return (
    <SnackbarContent
      ref={ref}
      className="mx-auto max-w-320 w-full relative pointer-events-auto py-4"
    >
      <NotificationCard item={item} onClose={props.onClose} />
    </SnackbarContent>
  );
});

NotificationTemplate.propTypes = {
  item: PropTypes.shape({
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
    id: PropTypes.number,
    message: PropTypes.string,
    options: PropTypes.shape({
      variant: PropTypes.string,
    }),
  }).isRequired,
  onClose: PropTypes.func.isRequired,
};

export default NotificationTemplate;
