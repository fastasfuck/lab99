import PropTypes from 'prop-types';
import Icon from '@mui/material/Icon';
import IconButton from '@mui/material/IconButton';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useDispatch, useSelector } from 'react-redux';
import { setDefaultSettings } from 'app/store/fuse/settingsSlice';
import _ from 'lodash';
/* eslint-disable import/extensions */
import { navbarToggleMobile, navbarToggle } from '../../store/fuse/navbarSlice';
/* eslint-enable import/extensions */

function NavbarToggleButton({ className, children }) {
  const dispatch = useDispatch();
  const theme = useTheme();
  const mdDown = useMediaQuery(theme.breakpoints.down('lg'));
  const settings = useSelector(({ fuse }) => fuse.settings.current);
  const { config } = settings.layout;

  return (
    <IconButton
      className={className}
      color="inherit"
      size="small"
      onClick={() => {
        if (mdDown) {
          dispatch(navbarToggleMobile());
        } else if (config.navbar.style === 'style-2') {
          dispatch(
            setDefaultSettings(
              _.set({}, 'layout.config.navbar.folded', !settings.layout.config.navbar.folded)
            )
          );
        } else {
          dispatch(navbarToggle());
        }
      }}
    >
      {children}
    </IconButton>
  );
}

NavbarToggleButton.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  className: PropTypes.string.isRequired,
};

NavbarToggleButton.defaultProps = {
  children: (
    <Icon fontSize="inherit" className="text-16">
      menu_open
    </Icon>
  ),
};

export default NavbarToggleButton;
