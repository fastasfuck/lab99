import { forwardRef } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

const NavLinkAdapter = forwardRef(
  ({
    activeClassName, className, to, children, ...props
  }, ref) => (
    <NavLink ref={ref} to={to} className={className}>
      {children}
    </NavLink>
  ),
);

NavLinkAdapter.propTypes = {
  activeClassName: PropTypes.string,
  className: PropTypes.string,
  to: PropTypes.string,
  children: PropTypes.node,
};

export default NavLinkAdapter;
