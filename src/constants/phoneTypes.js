const PHONE_TYPES = {
  BASE: 'base',
  PRIMARY: 'primary',
  SECONDARY: 'secondary',
};

export default PHONE_TYPES;
