import { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { TextField, Button, Grid } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import * as Yup from 'yup';
import capitalize from 'lodash/capitalize';
import { getTitlesRequest } from 'features/profile/api/index';

const validationSchema = Yup.object().shape({
  title: Yup.string().required('Title is required'),
  estate_name: Yup.string().required('Estate name is required'),
  social_security_number: Yup.string().required('Social Security Number name is required'),
  middle_name: Yup.string().required('Middle Name is required'),
  listing_ownership: Yup.string().required('Listing ownership is required'),
  work: Yup.string().required('Work is required'),
  employer_name: Yup.string().required('Employer name name is required'),
  birth_date: Yup.date().required('Birth date is required'),
});

function GeneralForm({ onSubmit, data }) {
  const [loadingTitle, setLoadingTitle] = useState(false);
  const [attemptTitle, setAttemptTitle] = useState(false);
  const [titleOptions, setTitlesOptions] = useState(['mr', 'mrs', 'miss', 'ms', 'dr']);
  const formik = useFormik({
    initialValues: {
      title: '',
      estate_name: '',
      social_security_number: '',
      middle_name: '',
      listing_ownership: '',
      work: '',
      employer_name: '',
      birth_date: '',
    },
    validationSchema,
    validateOnMount: false,
    validateOnChange: false,
    onSubmit,
  });

  useEffect(() => {
    if (data && !formik.dirty) {
      formik.setValues(data?.profile);
    }
  }, [data]);

  const handleOpenSelect = async () => {
    try {
      if (!attemptTitle) {
        setLoadingTitle(true);
        setAttemptTitle(true);
        const { data: responseData } = await getTitlesRequest();

        setTitlesOptions(responseData);
      }
    } catch (error) {
      toast.error(error.message);
    } finally {
      setLoadingTitle(false);
    }
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container direction="column" spacing={1}>
        <Grid item>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Title</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="title"
              sx={{ width: '400px' }}
              onOpen={handleOpenSelect}
              label="Title"
              placeholder="Title"
              error={formik.errors.title && Boolean(formik.errors.title)}
              onChange={formik.handleChange}
              IconComponent={() =>
                loadingTitle ? (
                  <CircularProgress />
                ) : (
                  <i className="material-icons">arrow_drop_down</i>
                )
              }
              value={String(formik.values?.title)}
            >
              {titleOptions.map((title) => (
                <MenuItem key={title} value={title}>
                  {capitalize(title)}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>{formik.touched.title && formik.errors.title}</FormHelperText>
          </FormControl>
        </Grid>
        <Grid item>
          <TextField
            sx={{ width: '400px' }}
            name="estate_name"
            label="Estate Name"
            error={formik.errors.estate_name && Boolean(formik.errors.estate_name)}
            helperText={formik.touched.estate_name && formik.errors.estate_name}
            onChange={formik.handleChange}
            value={String(formik.values?.estate_name)}
          />
        </Grid>
        <Grid item>
          <TextField
            sx={{ width: '400px' }}
            name="middle_name"
            label="Middle Name"
            error={formik.errors.middle_name && Boolean(formik.errors.middle_name)}
            helperText={formik.touched.middle_name && formik.errors.middle_name}
            onChange={formik.handleChange}
            value={String(formik.values?.middle_name)}
          />
        </Grid>
        <Grid item>
          <TextField
            sx={{ width: '400px' }}
            name="work"
            label="Work"
            error={formik.errors.work && Boolean(formik.errors.work)}
            helperText={formik.touched.work && formik.errors.work}
            onChange={formik.handleChange}
            value={String(formik.values?.work)}
          />
        </Grid>
        <Grid item>
          <TextField
            sx={{ width: '400px' }}
            name="listing_ownership"
            label="Listing ownership"
            error={formik.errors.listing_ownership && Boolean(formik.errors.listing_ownership)}
            helperText={formik.touched.listing_ownership && formik.errors.listing_ownership}
            onChange={formik.handleChange}
            value={String(formik.values?.listing_ownership)}
          />
        </Grid>
        <Grid item>
          <TextField
            sx={{ width: '400px' }}
            name="social_security_number"
            label="Social Security Number"
            error={
              formik.errors.social_security_number && Boolean(formik.errors.social_security_number)
            }
            helperText={
              formik.touched.social_security_number && formik.errors.social_security_number
            }
            onChange={formik.handleChange}
            value={String(formik.values?.social_security_number)}
          />
        </Grid>
        <Grid item>
          <TextField
            sx={{ width: '400px' }}
            name="employer_name"
            label="Employer name"
            error={formik.errors.employer_name && Boolean(formik.errors.employer_name)}
            helperText={formik.touched.employer_name && formik.errors.employer_name}
            onChange={formik.handleChange}
            value={String(formik.values?.employer_name)}
          />
        </Grid>
        <Grid item>
          <DesktopDatePicker
            name="birth_date"
            label="Date desktop"
            inputFormat="MM/dd/yyyy"
            error={formik.errors.birth_date && Boolean(formik.errors.birth_date)}
            helperText={formik.touched.birth_date && formik.errors.birth_date}
            onChange={(date) => formik.setFieldValue('birth_date', date)}
            value={formik.values?.birth_date}
            renderInput={(params) => <TextField sx={{ width: '400px' }} {...params} />}
          />
        </Grid>
        <Grid item>
          <Button variant="contained" type="submit">
            Save
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

GeneralForm.displayName = 'GeneralForm';

GeneralForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  data: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default GeneralForm;
