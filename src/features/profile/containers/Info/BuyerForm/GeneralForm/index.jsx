import { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import { TextField, Button, Grid } from '@mui/material';
import * as Yup from 'yup';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import { getTitlesRequest } from 'features/profile/api/index';
import MenuItem from '@mui/material/MenuItem';
import capitalize from 'lodash/capitalize';
import CircularProgress from '@mui/material/CircularProgress';

const validationSchema = Yup.object().shape({
  company: Yup.string().required('Company is required'),
  title: Yup.string().required('Title is required'),
});

function GeneralForm({ onSubmit, data }) {
  const [attemptTitle, setAttemptTitle] = useState(false);
  const [loadingTitle, setLoadingTitle] = useState(false);
  const [titleOptions, setTitlesOptions] = useState(['mr', 'mrs', 'miss', 'ms', 'dr']);

  const formik = useFormik({
    initialValues: {
      title: '',
      company: '',
    },
    validationSchema,
    validateOnMount: false,
    validateOnChange: false,
    onSubmit,
  });

  useEffect(() => {
    if (data && !formik.dirty) {
      formik.setValues(data?.profile);
    }
  }, [data]);

  const handleOpenSelect = async () => {
    try {
      if (!attemptTitle) {
        setLoadingTitle(true);
        setAttemptTitle(true);
        const { data: responseData } = await getTitlesRequest();

        setTitlesOptions(responseData);
      }
    } catch (error) {
      toast.error(error.message);
    } finally {
      setLoadingTitle(false);
    }
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid
        container
        direction="column"
        justifyContent="flex-start"
        alignItems="flex-start"
        spacing={1}
      >
        <Grid item>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Title</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="title"
              sx={{ width: '400px' }}
              onOpen={handleOpenSelect}
              label="Title"
              placeholder="Title"
              error={formik.errors.title && Boolean(formik.errors.title)}
              onChange={(e) => {
                const { name, value } = e.target;
                formik.setFieldValue(name, value);
              }}
              IconComponent={() =>
                loadingTitle ? (
                  <CircularProgress />
                ) : (
                  <i className="material-icons">arrow_drop_down</i>
                )
              }
              value={String(formik.values?.title)}
            >
              {titleOptions.map((title) => (
                <MenuItem key={title} value={title}>
                  {capitalize(title)}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item>
          <TextField
            name="company"
            label="Company"
            error={formik.errors.company && Boolean(formik.errors.company)}
            helperText={formik.touched.company && formik.errors.company}
            onChange={formik.handleChange}
            value={String(formik.values?.company)}
          />
        </Grid>
        <Grid item>
          <Button variant="contained" type="submit">
            Save
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

GeneralForm.displayName = 'GeneralForm';

GeneralForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  data: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default GeneralForm;
