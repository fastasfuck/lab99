import { Avatar, Box } from '@mui/material';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';

const Input = styled('input')({
  display: 'none',
});

function ChangeAvatarForm({ onUpload, photo, onDelete }) {
  return (
    <Box sx={{ display: 'flex' }}>
      <div>
        <Avatar src={photo} sx={{ width: '100px', height: '100px' }} />
      </div>
      <div>
        <Input accept="image/*" multiple type="file" onChange={onUpload} />
        <Button variant="contained" component="span">
          Upload
        </Button>
        <Button variant="outlined" color="error" onClick={onDelete}>
          Remove
        </Button>
      </div>
    </Box>
  );
}

ChangeAvatarForm.displayName = 'ChangeAvatarForm';

ChangeAvatarForm.propTypes = {
  onUpload: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  photo: PropTypes.string.isRequired,
};

export default ChangeAvatarForm;
