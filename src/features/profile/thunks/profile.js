import { createAsyncThunk } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import * as api from 'features/profile/api';

const getErrorMessage = (error) => {
  if (error.response?.status === 422) {
    return Object.keys(error.response.data)
      .map((key) => error.response.data[key])
      .join(', ');
  }
  return error.message;
};

export const uploadAvatarThunk = createAsyncThunk(
  'profile/uploadAvatarThunk',
  async (file, thunkApi) => {
    const { rejectWithValue, getState } = thunkApi;

    try {
      const { token } = getState().auth;
      const formData = new FormData();
      formData.append('file', file);
      const data = await api.uploadAvatarRequest({ token, file: formData });
      toast.success('Uploaded');
      return data;
    } catch (error) {
      toast.error(`Error ${error.message}`);
      return rejectWithValue(error);
    }
  }
);

export const deleteAvatarThunk = createAsyncThunk(
  'profile/deleteAvatarThunk',
  async (_, thunkApi) => {
    const { rejectWithValue, getState } = thunkApi;

    try {
      const { token } = getState().auth;

      const data = await api.deleteAvatarRequest({
        token,
      });
      toast.success('Avatar has been removed');
      return data;
    } catch (error) {
      toast.error(`Error ${error.message}`);
      return rejectWithValue(error);
    }
  }
);

export const updateProfileThunk = createAsyncThunk(
  'profile/updateProfileThunk',
  async (profile, thunkApi) => {
    const { rejectWithValue, getState } = thunkApi;

    try {
      const { token, user } = getState().auth;

      const data = await api.updateProfileRequest({
        token,
        profile,
        id: user.id,
      });
      toast.success('Profile has been updated');
      return data;
    } catch (error) {
      const messages = getErrorMessage(error);
      toast.error(messages);
      return rejectWithValue(error);
    }
  }
);

export const updateSellerProfileThunk = createAsyncThunk(
  'profile/updateSellerProfileThunk',
  async (profile, thunkApi) => {
    const { rejectWithValue, getState } = thunkApi;

    try {
      const { token, user } = getState().auth;

      const data = await api.updateSellerProfileRequest({
        token,
        profile,
        id: user.id,
      });
      toast.success('Profile has been updated');
      return data;
    } catch (error) {
      const messages = getErrorMessage(error);
      toast.error(messages);
      return rejectWithValue(error);
    }
  }
);

export const updateBuyerProfileThunk = createAsyncThunk(
  'profile/updateBuyerProfileThunk',
  async (profile, thunkApi) => {
    const { rejectWithValue, getState } = thunkApi;

    try {
      const { token, user } = getState().auth;

      const data = await api.updateBuyerProfileRequest({
        token,
        profile,
        id: user.id,
      });
      toast.success('Profile has been updated');
      return data;
    } catch (error) {
      const messages = getErrorMessage(error);
      toast.error(messages);
      return rejectWithValue(error);
    }
  }
);

export const changePasswordThunk = createAsyncThunk(
  'profile/changePasswordThunk',
  async (passwords, thunkApi) => {
    const { rejectWithValue, getState } = thunkApi;

    try {
      const { token, user } = getState().auth;

      const data = await api.updateProfileRequest({
        token,
        profile: passwords,
        id: user.id,
      });
      toast.success('Profile has been updated');
      return data;
    } catch (error) {
      const messages = getErrorMessage(error);
      toast.error(messages);
      return rejectWithValue(error);
    }
  }
);
