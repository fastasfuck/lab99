import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import Logo from 'assets/images/logos/logo-alhamize.png';

import styles from './styles.module.css';

const cx = classNames.bind(styles);

function JoinTemplate({ children }) {
  return (
    <div className="grid grid-cols-2 w-full">
      {children}
      <div className={classNames('bg-blue-900', cx('bg-image'))}>
        <div className={cx('container')}>
          <div className={cx('logo-container')}>
            <img alt="logo" src={Logo} className={cx('logo')} />
          </div>
        </div>
        <div className={cx('bg-gradient')} />
      </div>
    </div>
  );
}

JoinTemplate.displayName = 'JoinTemplate';

JoinTemplate.propTypes = {
  children: PropTypes.node.isRequired,
};

export default JoinTemplate;
