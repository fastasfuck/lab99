const ROLES = {
  Buyer: 'buyer',
  Seller: 'seller',
  Admin: 'administrator',
};

export default ROLES;
