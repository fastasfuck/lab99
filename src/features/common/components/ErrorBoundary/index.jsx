import PropTypes from 'prop-types';
import React from 'react';
import CenterTemplate from 'features/common/templates/CenterTemplate';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch() {
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, errorInfo)
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;

    if (hasError) {
      return (
        <CenterTemplate>
          <div>
            <h1>Something went wrong.</h1>
            <button type="button" onClick={() => window.location.reload()}>
              Reload page
            </button>
          </div>
        </CenterTemplate>
      );
    }

    return children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

export default ErrorBoundary;
