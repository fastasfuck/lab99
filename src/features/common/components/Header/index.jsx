import { ThemeProvider } from '@mui/material/styles';
import PropTypes from 'prop-types';

import AppBar from '@mui/material/AppBar';
import clsx from 'clsx';

import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { useSelector } from 'react-redux';
import { selectToolbarTheme } from 'features/common/slices/fuse/settingsSlice';

import UserMenu from './UserMenu';

function Header({ onDrawerOpen, open }) {
  const toolbarTheme = useSelector(selectToolbarTheme);

  return (
    <ThemeProvider theme={toolbarTheme}>
      <AppBar
        id="fuse-toolbar"
        className={clsx('fixed top-0 flex z-20 shadow-md')}
        color="default"
        position="static"
        style={{ backgroundColor: toolbarTheme.palette.background.paper }}
      >
        <Toolbar className="p-0 min-h-48 md:min-h-64">
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={onDrawerOpen}
            edge="start"
            sx={{
              marginRight: '36px',
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }} />

          <Box sx={{ flexGrow: 0 }}>
            <UserMenu />
          </Box>
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  );
}

Header.displayName = 'Header';

Header.propTypes = {
  onDrawerOpen: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default Header;
