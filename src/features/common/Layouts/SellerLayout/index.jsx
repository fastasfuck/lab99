import PropTypes from 'prop-types';
import HomeTemplate from 'features/common/templates/HomeTemplate';
import Header from 'features/common/components/Header';
import { withOpenDrawer } from 'features/common/hocs/withOpenDrawer';
import SellerDrawerList from 'features/common/components/SellerDrawerList';

const SellerLayout = withOpenDrawer(({ children, open, onOpenDrawer, onCloseDrawer }) => (
  <HomeTemplate
    open={open}
    onCloseDrawer={onCloseDrawer}
    onOpenDrawer={onOpenDrawer}
    DrawerChildren={<SellerDrawerList />}
    HeaderChildren={<Header open={open} onDrawerOpen={onOpenDrawer} />}
  >
    {children}
  </HomeTemplate>
));

SellerLayout.displayName = 'SellerLayout';

SellerLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default SellerLayout;
