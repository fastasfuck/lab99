import { createAsyncThunk } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import * as auth from 'features/common/api';
import { requestEndAction, requestStartAction } from '../actions/api';

export const createListingThunk = createAsyncThunk(
  'auth/createlistingThunk',
  async (payload, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;
    try {
      dispatch(requestStartAction());
      const listingResponse = await auth.createListingRequest(payload);
      dispatch(requestEndAction());
      return listingResponse;
    } catch (error) {
      dispatch(requestEndAction());
      const errorsText = [];
      Object.values(error.response.data).forEach((errors) => {
        errors.forEach((errorText) => {
          errorsText.push(errorText);
        });
      });
      toast.error(errorsText.join(', '));
      return rejectWithValue(error);
    }
  }
);

export const updateListingThunk = createAsyncThunk(
  'auth/updatelistingThunk',
  async (payload, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;
    try {
      dispatch(requestStartAction());
      const { listingId, data } = payload;
      const listingResponse = await auth.updateListingRequest(listingId, data);
      dispatch(requestEndAction());
      return listingResponse;
    } catch (error) {
      dispatch(requestEndAction());
      const errorsText = [];
      Object.values(error.response.data).forEach((errors) => {
        errors.forEach((errorText) => {
          errorsText.push(errorText);
        });
      });
      toast.error(errorsText.join(', '));
      return rejectWithValue(error);
    }
  }
);

export const getContractPreviewThunk = createAsyncThunk(
  'auth/contractPreviewThunk',
  async (payload, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;
    try {
      dispatch(requestStartAction());
      const data = await (payload.listingId ? auth.getContractByListingId(payload.listingId) : auth.getContractPreview(payload));
      dispatch(requestEndAction());
      return data;
    } catch (error) {
      dispatch(requestEndAction());
      const errorsText = [];
      Object.values(error.response.data).forEach((errors) => {
        errors.forEach((errorText) => {
          errorsText.push(errorText);
        });
      });
      toast.error(errorsText.join(', '));
      return rejectWithValue(error);
    }
  }
);

export const getListingsThunk = createAsyncThunk(
  'auth/getListingsThunk',
  async (params, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;
    try {
      dispatch(requestStartAction());
      const data = await auth.getListings(params);
      dispatch(requestEndAction());
      return data;
    } catch (error) {
      dispatch(requestEndAction());
      const errorsText = [];
      Object.values(error.response.data).forEach((errors) => {
        errors.forEach((errorText) => {
          errorsText.push(errorText);
        });
      });
      toast.error(errorsText.join(', '));
      return rejectWithValue(error);
    }
  }
);

export const getListingByIdThunk = createAsyncThunk(
  'auth/getListingByIdThunk',
  async (listingId, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;
    try {
      dispatch(requestStartAction());
      const data = await auth.getListingById(listingId);
      dispatch(requestEndAction());
      return data;
    } catch (error) {
      dispatch(requestEndAction());
      const errorsText = [];
      Object.values(error.response.data).forEach((errors) => {
        errors.forEach((errorText) => {
          errorsText.push(errorText);
        });
      });
      toast.error(errorsText.join(', '));
      return rejectWithValue(error);
    }
  }
);

export const deleteListingByIdThunk = createAsyncThunk(
  'auth/deleteListingByIdThunk',
  async (listingId, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;
    try {
      dispatch(requestStartAction());
      const data = await auth.deleteListingById(listingId);
      dispatch(requestEndAction());
      return data;
    } catch (error) {
      dispatch(requestEndAction());
      const errorsText = [];
      Object.values(error.response.data).forEach((errors) => {
        errors.forEach((errorText) => {
          errorsText.push(errorText);
        });
      });
      toast.error(errorsText.join(', '));
      return rejectWithValue(error);
    }
  }
);