import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { selectIsAuthenticated } from 'features/common/slices/auth';
import paths from 'app/Routes/paths';

const withPrivateRoute = (Component) =>
  function (props) {
    const isAuthenticated = useSelector(selectIsAuthenticated);

    if (isAuthenticated) {
      return <Component {...props} />;
    }

    return <Navigate to={paths.signIn()} />;
  };

export default withPrivateRoute;
