import PropTypes from 'prop-types';
import FuseNavigation from '@fuse/core/FuseNavigation';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import clsx from 'clsx';
import { memo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectNavigation } from 'app/store/fuse/navigationSlice';
/* eslint-disable import/extensions */
import { navbarCloseMobile } from '../../store/fuse/navbarSlice';
/* eslint-enable import/extensions */

function Navigation({ className, layout, dense, active }) {
  const navigation = useSelector(selectNavigation);
  const theme = useTheme();
  const mdDown = useMediaQuery(theme.breakpoints.down('lg'));
  const dispatch = useDispatch();

  function handleItemClick() {
    if (mdDown) {
      dispatch(navbarCloseMobile());
    }
  }

  return (
    <FuseNavigation
      className={clsx('navigation', className)}
      navigation={navigation}
      layout={layout}
      dense={dense}
      active={active}
      onItemClick={handleItemClick}
    />
  );
}

Navigation.propTypes = {
  active: PropTypes.bool.isRequired,
  className: PropTypes.string.isRequired,
  dense: PropTypes.bool.isRequired,
  layout: PropTypes.string,
};

Navigation.defaultProps = {
  layout: 'vertical',
};

export default memo(Navigation);
