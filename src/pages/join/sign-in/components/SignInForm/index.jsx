import { useFormik } from 'formik';
import { useSelector } from 'react-redux';
import classNames from 'classnames/bind';
import { TextField, Checkbox, FormControlLabel } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { checkIfIsLoading } from 'features/common/slices/api/index';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

const validationSchema = Yup.object().shape({
  email: Yup.string('Enter your email').email('Enter a valid email').required('Email is required'),
  password: Yup.string('Enter your password')
    .min(8, 'Password should be of minimum 8 characters length')
    .required('Password is required'),
});

function SignInForm({ onSubmit }) {
  const isLoading = useSelector(checkIfIsLoading);
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      rememberMe: false,
    },
    validationSchema,
    validateOnMount: false,
    validateOnChange: false,
    onSubmit,
  });
  return (
    <form onSubmit={formik.handleSubmit} className={cx('form')}>
      <TextField
        name="email"
        label="Email"
        type="email"
        error={formik.errors.email && Boolean(formik.errors.email)}
        helperText={formik.touched.email && formik.errors.email}
        onChange={formik.handleChange}
        value={String(formik.values?.email)}
      />
      <TextField
        name="password"
        label="Password"
        type="password"
        error={formik.errors.password && Boolean(formik.errors.password)}
        helperText={formik.touched.password && formik.errors.password}
        onChange={formik.handleChange}
        value={formik.values.password}
      />
      <div className="flex justify-between items-center">
        <FormControlLabel
          value="rememberMe"
          onChange={formik.handleChange}
          checked={formik.values.rememberMe}
          control={<Checkbox />}
          label="Remember me"
          name="rememberMe"
        />
        <LoadingButton loading={isLoading} color="secondary" variant="contained" type="submit">
          Sign In
        </LoadingButton>
      </div>
    </form>
  );
}

SignInForm.displayName = 'SignInForm';

SignInForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default SignInForm;
