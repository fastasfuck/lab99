import { Link } from 'react-router-dom';
import classNames from 'classnames/bind';
import { Helmet } from 'react-helmet';
import { useDispatch } from 'react-redux';
import Typography from '@mui/material/Typography';
import { Divider } from '@mui/material';

import paths from 'app/Routes/paths';
import JoinTemplate from 'features/common/templates/JoinTemplate';
import { authThunk } from 'features/common/thunks/auth';

import SignInForm from './components/SignInForm';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

/* eslint-disable import/prefer-default-export */
export function SignInPage() {
  const dispatch = useDispatch();

  const handleSubmit = async (values) => {
    await dispatch(authThunk(values));
  };

  return (
    <>
      <Helmet>
        <title>Alphamize | Sign in</title>
      </Helmet>
      <JoinTemplate>
        <section className="flex justify-center items-center">
          <div>
            <Typography variant="h2" gutterBottom>
              Sign In
            </Typography>
            <Typography variant="subtitle1" className="py-12" gutterBottom component="div">
              Enter your email address and password to access admin panel
            </Typography>
            <div className={cx('form')}>
              <SignInForm onSubmit={handleSubmit} />
            </div>
            <Divider />
            <footer>
              <div className="py-12">
                Don&apos;t have an account?&nbsp;
                <Link to={paths.signUp()}>Register Now!</Link>
              </div>
              <div>
                <Link to={paths.forgotPassword()}>Forgot your password?</Link>
              </div>
            </footer>
          </div>
        </section>
      </JoinTemplate>
    </>
  );
}

SignInPage.displayName = 'SignInPage';
