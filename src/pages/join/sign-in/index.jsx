import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';

const SignInPage = loadable(() => import(/* webpackChunkName: "join-page" */ './page'), {
  resolveComponent: ({ SignInPage: Page }) => Page,
  fallback: <Loading />,
});

export default SignInPage;
