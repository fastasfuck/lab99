import { Typography } from '@mui/material';
import paths from 'app/Routes/paths';
import CenterTemplate from 'features/common/templates/CenterTemplate';
import { Link } from 'react-router-dom';

function NotFoundPage() {
  return (
    <CenterTemplate>
      <div className="flex items-center flex-col">
        <Typography variant="h3">404 | Not Found</Typography>
        <Link to={paths.home()} className="pt-6">
          Go to Home
        </Link>
      </div>
    </CenterTemplate>
  );
}

NotFoundPage.displayName = 'NotFoundPage';

export default NotFoundPage;
