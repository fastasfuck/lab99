import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';

const BuyerHomePage = loadable(() => import(/* webpackChunkName: "buyer-page" */ './page'), {
  resolveComponent: ({ BuyerHomePage: Page }) => Page,
  fallback: <Loading />,
});

export default BuyerHomePage;
