import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { logout } from 'features/common/slices/auth';
import Loading from 'features/common/components/Loading';

export default function LogoutPage() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(logout());
  }, []);

  return <Loading />;
}

LogoutPage.displayName = 'LogoutPage';
