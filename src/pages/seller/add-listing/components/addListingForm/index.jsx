import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import { Box, Button, Grid, TextField, Typography } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import InputAdornment from '@mui/material/InputAdornment';
import LoadingButton from '@mui/lab/LoadingButton';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import removeIcon from 'assets/images/icons/remove.png';
import classNames from 'classnames/bind';
import { getCities, getStates } from 'features/profile/api/index';
import capitalize from 'lodash/capitalize';
import { useEffect, useState } from 'react';
import ImageUploading from 'react-images-uploading';
import LISTING_STATUSES from 'src/constants/listingStatuses';
import styles from './styles.module.css';
import { useSelector } from 'react-redux';
import { checkIfIsLoading } from 'features/common/slices/api/index';

const cx = classNames.bind(styles);

function AddListingForm({ form, buttons }) {
  const [states, setStates] = useState(null);
  const [cities, setCities] = useState(null);
  const isLoading = useSelector(checkIfIsLoading);

  useEffect(async () => {
    const { data } = await getStates();
    setStates(data.states);
  }, []);

  const onImageChange = (values) => {
    form.setFieldValue('photos', values);
  };

  const handleOpenCitySelect = async () => {
    const stateId = states.find((state) => state.name === form.values.address.state).id;
    const { data } = await getCities(stateId);
    setCities(data.cities);
  };

  return (
    <form onSubmit={form.handleSubmit}>
      <Grid container columns={12} columnSpacing={2} rowSpacing={2}>
        <Grid item xs={12}>
          <Typography variant="subtitle2">Address Information</Typography>
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="address.lineOne"
            label="Address line 1"
            fullWidth
            error={form.errors.address?.lineOne && Boolean(form.errors.address?.lineOne)}
            helperText={form.touched.address?.lineOne && form.errors.address?.lineOne}
            onChange={form.handleChange}
            value={form.values.address.lineOne}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="address.lineTwo"
            label="Address line 2"
            fullWidth
            error={form.errors.address?.lineTwo && Boolean(form.errors.address?.lineTwo)}
            helperText={form.touched.address?.lineTwo && form.errors.address?.lineTwo}
            onChange={form.handleChange}
            value={form.values.address.lineTwo}
          />
        </Grid>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <InputLabel>State</InputLabel>
            <Select
              name="address.state"
              label="State"
              error={form.errors.address?.state && Boolean(form.errors.address?.state)}
              onChange={(e) => {
                form.setFieldValue('address.city', '');
                form.handleChange(e);
              }}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.address?.state)}
            >
              <MenuItem disabled value="">
                <span>State</span>
              </MenuItem>
              {states &&
                states.map((state) => (
                  <MenuItem key={state.id} value={state.name}>
                    {capitalize(state.name)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.address?.state && form.errors.address?.state}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <InputLabel>City</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="address.city"
              onOpen={handleOpenCitySelect}
              label="City"
              disabled={!form.values.address.state}
              error={form.errors.address?.city && Boolean(form.errors.address?.city)}
              onChange={form.handleChange}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.address?.city)}
            >
              <MenuItem disabled value="">
                <span>City</span>
              </MenuItem>
              {cities &&
                cities.map((city) => (
                  <MenuItem key={city.id} value={city.name}>
                    {capitalize(city.name)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.address?.city && form.errors.address?.city}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="address.zipCode"
            label="Zip code"
            fullWidth
            error={form.errors.address?.zipCode && Boolean(form.errors.address?.zipCode)}
            helperText={form.touched.address?.zipCode && form.errors.address?.zipCode}
            onChange={form.handleChange}
            value={form.values.address.zipCode}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="subtitle2">Prices</Typography>
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.cost"
            label="Cost basis"
            InputProps={{
              startAdornment: <InputAdornment position="start">$</InputAdornment>,
              inputMode: 'numeric',
              pattern: '[0-9]*',
            }}
            fullWidth
            error={form.errors.listing?.cost && Boolean(form.errors.listing?.cost)}
            helperText={form.touched.listing?.cost && form.errors.listing?.cost}
            onChange={form.handleChange}
            value={form.values.listing?.cost}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.marketPrice"
            label="Market price"
            InputProps={{
              startAdornment: <InputAdornment position="start">$</InputAdornment>,
              inputMode: 'numeric',
              pattern: '[0-9]*',
            }}
            fullWidth
            error={form.errors.listing?.marketPrice && Boolean(form.errors.listing?.marketPrice)}
            helperText={form.touched.listing?.marketPrice && form.errors.listing?.marketPrice}
            onChange={form.handleChange}
            value={form.values.listing?.marketPrice}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.currentPrice"
            label="Current price"
            InputProps={{
              startAdornment: <InputAdornment position="start">$</InputAdornment>,
              inputMode: 'numeric',
              pattern: '[0-9]*',
            }}
            fullWidth
            error={form.errors.listing?.currentPrice && Boolean(form.errors.listing?.currentPrice)}
            helperText={form.touched.listing?.currentPrice && form.errors.listing?.currentPrice}
            onChange={form.handleChange}
            value={form.values.listing?.currentPrice}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.askingPrice"
            label="Seller asking price"
            InputProps={{
              startAdornment: <InputAdornment position="start">$</InputAdornment>,
              inputMode: 'numeric',
              pattern: '[0-9]*',
            }}
            fullWidth
            error={form.errors.listing?.askingPrice && Boolean(form.errors.listing?.askingPrice)}
            helperText={form.touched.listing?.askingPrice && form.errors.listing?.askingPrice}
            onChange={form.handleChange}
            value={form.values.listing?.askingPrice}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.optionPrice"
            label="Option price"
            InputProps={{
              startAdornment: <InputAdornment position="start">$</InputAdornment>,
              inputMode: 'numeric',
              pattern: '[0-9]*',
            }}
            fullWidth
            error={form.errors.listing?.optionPrice && Boolean(form.errors.listing?.optionPrice)}
            helperText={form.touched.listing?.optionPrice && form.errors.listing?.optionPrice}
            onChange={form.handleChange}
            value={form.values.listing?.optionPrice}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="subtitle2">Property information</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="listing.propertyInformation"
            label="Property Information"
            fullWidth
            multiline
            rows={5}
            error={
              form.errors.listing?.propertyInformation &&
              Boolean(form.errors.listing?.propertyInformation)
            }
            helperText={
              form.touched.listing?.propertyInformation && form.errors.listing?.propertyInformation
            }
            onChange={form.handleChange}
            value={form.values.listing?.propertyInformation}
          />
        </Grid>
        <Grid item xs={4}>
          <DesktopDatePicker
            name="listing.biddingEnds"
            label="Bidding ends"
            inputFormat="MM/dd/yyyy"
            renderInput={(params) => (
              <TextField
                fullWidth
                {...params}
                error={
                  form.errors.listing?.biddingEnds && Boolean(form.errors.listing?.biddingEnds)
                }
              />
            )}
            onChange={(value) => form.setFieldValue('listing.biddingEnds', value)}
            value={form.values.listing?.biddingEnds}
          />
          <FormHelperText error>
            {form.touched.listing?.biddingEnds && form.errors.listing?.biddingEnds}
          </FormHelperText>
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.propertySize"
            label="Property size"
            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
            fullWidth
            error={form.errors.listing?.propertySize && Boolean(form.errors.listing?.propertySize)}
            helperText={form.touched.listing?.propertySize && form.errors.listing?.propertySize}
            onChange={form.handleChange}
            value={form.values.listing?.propertySize}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.bedrooms"
            label="Number of bedrooms"
            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
            fullWidth
            error={form.errors.listing?.bedrooms && Boolean(form.errors.listing?.bedrooms)}
            helperText={form.touched.listing?.bedrooms && form.errors.listing?.bedrooms}
            onChange={form.handleChange}
            value={form.values.listing?.bedrooms}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.bathrooms"
            label="Number of bathrooms"
            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
            fullWidth
            error={form.errors.listing?.bathrooms && Boolean(form.errors.listing?.bathrooms)}
            helperText={form.touched.listing?.bathrooms && form.errors.listing?.bathrooms}
            onChange={form.handleChange}
            value={form.values.listing?.bathrooms}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.garages"
            label="Number of garages"
            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
            fullWidth
            error={form.errors.listing?.garages && Boolean(form.errors.listing?.garages)}
            helperText={form.touched.listing?.garages && form.errors.listing?.garages}
            onChange={form.handleChange}
            value={form.values.listing?.garages}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.gardenSize"
            label="Garden size"
            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
            fullWidth
            error={form.errors.listing?.gardenSize && Boolean(form.errors.listing?.gardenSize)}
            helperText={form.touched.listing?.gardenSize && form.errors.listing?.gardenSize}
            onChange={form.handleChange}
            value={form.values.listing?.gardenSize}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.terraceSize"
            label="Terrace size"
            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
            fullWidth
            error={form.errors.listing?.terraceSize && Boolean(form.errors.listing?.terraceSize)}
            helperText={form.touched.listing?.terraceSize && form.errors.listing?.terraceSize}
            onChange={form.handleChange}
            value={form.values.listing?.terraceSize}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.yearBuilt"
            label="Year built"
            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
            fullWidth
            error={form.errors.listing?.yearBuilt && Boolean(form.errors.listing?.yearBuilt)}
            helperText={form.touched.listing?.yearBuilt && form.errors.listing?.yearBuilt}
            onChange={form.handleChange}
            value={form.values.listing?.yearBuilt}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="listing.postalCode"
            label="Postal code"
            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
            fullWidth
            error={form.errors.listing?.postalCode && Boolean(form.errors.listing?.postalCode)}
            helperText={form.touched.listing?.postalCode && form.errors.listing?.postalCode}
            onChange={form.handleChange}
            value={form.values.listing?.postalCode}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="subtitle2">Photos</Typography>
          <ImageUploading
            multiple
            onChange={onImageChange}
            value={form.values.photos}
            acceptType={['jpg', 'gif', 'png', 'jpeg']}
          >
            {({ imageList, onImageUpload, onImageRemove, dragProps, errors }) => {
              return (
                <Box className={cx('upload-image-container')}>
                  <Button
                    className={cx('upload-button')}
                    variant={'outlined'}
                    onClick={onImageUpload}
                    {...dragProps}
                  >
                    Click or Drop here
                  </Button>
                  {errors && errors.acceptType && (
                    <Typography className={cx('errors')} color="error">
                      Wrong format of selected file. Accepted extensions: jpg, gif, png, jpeg
                    </Typography>
                  )}
                  <Box className={cx('image-container')}>
                    {imageList.map((image, index) => (
                      <div key={index} className={cx('image-item')}>
                        <img src={image.dataURL} alt="Listing image" />
                        <img
                          className={cx('remove-icon')}
                          onClick={() => onImageRemove(index)}
                          src={removeIcon}
                          alt="Remove listing"
                        />
                      </div>
                    ))}
                  </Box>
                </Box>
              );
            }}
          </ImageUploading>
        </Grid>
        <Grid item xs={4}>
          <FormControl fullWidth>
            <InputLabel>Status</InputLabel>
            <Select
              name="listing.status"
              label="Status"
              error={form.errors.listing?.status && Boolean(form.errors.listing?.status)}
              onChange={form.handleChange}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.listing?.status)}
            >
              <MenuItem disabled value="">
                <span>Status</span>
              </MenuItem>
              {Object.values(LISTING_STATUSES).map((status) => (
                <MenuItem key={status} value={status}>
                  {status}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText error>
              {form.touched.listing?.status && form.errors.listing?.status}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          {buttons &&
            buttons.map((button) => {
              const ButtonComponent = button.withLoader ? LoadingButton : Button;
              return (
                <ButtonComponent
                  loading={isLoading}
                  onClick={button.onClick}
                  type={button.type}
                  variant="secondary"
                >
                  {button.text}
                </ButtonComponent>
              );
            })}
        </Grid>
      </Grid>
    </form>
  );
}

AddListingForm.displayName = 'AddListingForm';

export default AddListingForm;
