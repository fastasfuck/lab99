import { useState, useEffect } from 'react';
import {
  Typography,
  Card,
  CardContent,
  Box,
  Grid,
  CardActions,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  TextField,
} from '@mui/material';
import ReactPaginate from 'react-paginate';
import { useDispatch } from 'react-redux';
import { deleteListingByIdThunk, getListingsThunk } from 'features/common/thunks/listing';
import imagePlaceholder from 'assets/images/image-placeholder.jpg';
import classNames from 'classnames/bind';
import styles from './styles.module.css';
import { Link } from 'react-router-dom';
import squareIcon from 'assets/images/icons/square.png';
import bedroomIcon from 'assets/images/icons/bedroom.png';
import buildIcon from 'assets/images/icons/build.png';
import { useSelector } from 'react-redux';
import { checkIfIsLoading } from 'features/common/slices/api/index';
import { selectCurrentUserRole } from 'features/common/slices/auth';
import Loading from 'features/common/components/Loading';
import { getFullAddress } from 'src/utils/addressFormatter';
import paths from '../../../app/Routes/paths';
import _, { isNull } from 'lodash';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import ROLES from 'features/common/enums';
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
  search: Yup.string().min(3, 'Enter at least 3 letters'),
});

const cx = classNames.bind(styles);

const ITEMS_PER_PAGE = 10;

/* eslint-disable import/prefer-default-export */
export function CurrentListingsPage() {
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [listingToDelete, setListingToDelete] = useState(null);
  const [selectedPage, setSelectedPage] = useState(0);
  const role = useSelector(selectCurrentUserRole);
  const isLoading = useSelector(checkIfIsLoading);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const getListings = async (offset, search='') => {
    const response = await dispatch(getListingsThunk({ take: ITEMS_PER_PAGE, skip: offset, ...(search && { search }) }));
    if (response.payload.success) {
      setCurrentItems(response.payload.data.list);
      setPageCount(Math.ceil(response.payload.data.total_count / ITEMS_PER_PAGE));
    }
  };

  const searchListings = values => {
    getListings(0, values.search);
  }

  const formik = useFormik({
    initialValues: {
      search: '',
    },
    validationSchema,
    validateOnMount: false,
    validateOnChange: false,
    onSubmit: searchListings,
  });

  const deleteListing = async (listingId) => {
    const response = await dispatch(deleteListingByIdThunk(listingId));
    if (response.payload.success) {
      setListingToDelete(null);
      // if the last item is deleted
      if (currentItems.length === 1) {
        setSelectedPage(selectedPage - 1);
      } else {
        getListings(selectedPage * ITEMS_PER_PAGE);
      }
    }
  };

  useEffect(async () => {
    getListings(selectedPage * ITEMS_PER_PAGE);
  }, [selectedPage]);

  const handlePageClick = async (event) => {
    setSelectedPage(event.selected);
  };

  return (
    <div className={cx('current-listings')}>
      <Typography variant="h3">Current Listings</Typography>
      {role === ROLES.Buyer && (
        <form onSubmit={formik.handleSubmit} className={cx('search-form')}>
          <Grid container alignItems="center" align="center">
            <Grid item xs={10}>
              <TextField
                name={'search'}
                label="Search listing by address"
                type="search"
                fullWidth
                error={formik.errors.search && Boolean(formik.errors.search)}
                helperText={formik.touched.search && formik.errors.search}
                onChange={formik.handleChange}
                value={formik.values.search}
              />
            </Grid>
            <Grid item xs={2}>
              <Button variant="contained" type="submit">
                Search
              </Button>
            </Grid>
          </Grid>
        </form>
      )}
      <Grid container rowSpacing={'16px'} spacing={'16px'} sx={{ mt: '16px' }}>
        {isLoading ? <Loading /> : _.isEmpty(currentItems) ? <Typography>No listings</Typography> :
          currentItems.map((item) => (
            <Grid key={item.id} item xl={3} md={4} sm={6}>
              <Card>
                <CardContent sx={{ p: 0, pb: 0 }}>
                  {item.photos && item.photos.length ? (
                    <img className={cx('image')} src={item.photos[0].url} alt="Listing image" />
                  ) : (
                    <img className={cx('image')} src={imagePlaceholder} alt="Listing image" />
                  )}
                  <Box sx={{ p: '1.75rem' }}>
                    <Link to={`${role === ROLES.Seller ? paths.sellerListing() : paths.buyerListing()}/${item.id}`}>
                      <Typography variant="h4" sx={{ fontWeight: 'bold', fontSize: '1.5rem' }}>
                        {getFullAddress(item.address)}
                      </Typography>
                    </Link>
                    <Box sx={{ display: 'flex', justifyContent: 'space-between', my: '16px' }}>
                      <Box sx={{ width: '30%', display: 'flex', alignItems: 'center' }}>
                        <img src={squareIcon} alt="square icon" className={cx('info-image')} />
                        <Typography sx={{ fontSize: '1.25rem' }} display="inline">
                          {item.property_size} Sq
                        </Typography>
                      </Box>
                      <Box sx={{ width: '30%', display: 'flex', alignItems: 'center' }}>
                        <img src={bedroomIcon} alt="bedroom icon" className={cx('info-image')} />
                        <Typography sx={{ fontSize: '1.25rem' }} display="inline">
                          {item.bedrooms}
                        </Typography>
                      </Box>
                      <Box sx={{ width: '30%', display: 'flex', alignItems: 'center' }}>
                        <img src={buildIcon} alt="building icon" className={cx('info-image')} />
                        <Typography sx={{ fontSize: '1.25rem' }} display="inline">
                          {item.year_built}
                        </Typography>
                      </Box>
                    </Box>
                    <Box
                      sx={{ display: 'flex', justifyContent: 'space-between', fontSize: '16px' }}
                    >
                      <Typography display="inline" sx={{ fontSize: '1.5rem' }}>
                        Asking Price
                      </Typography>
                      <Typography display="inline" sx={{ fontSize: '1.5rem' }}>
                        ${item.asking_price}
                      </Typography>
                    </Box>
                  </Box>
                </CardContent>
                {role === ROLES.Seller && <CardActions>
                  <Button size="small" color="error" onClick={() => setListingToDelete(item.id)}>
                    Delete
                  </Button>
                  <Button
                    size="small"
                    color="info"
                    onClick={() => navigate(`${paths.editListingBase()}/${item.id}`)}
                  >
                    Edit
                  </Button>
                </CardActions>}
              </Card>
            </Grid>
          ))}
      </Grid>
      <ReactPaginate
        breakLabel="..."
        nextLabel="Next >"
        onPageChange={handlePageClick}
        pageRangeDisplayed={5}
        className={cx('paginator')}
        pageClassName={cx('paginator-button')}
        previousClassName={cx('paginator-button')}
        nextClassName={cx('paginator-button')}
        disabledClassName={cx('disabled-paginator-button')}
        activeClassName={cx('active-paginator-button')}
        pageCount={pageCount}
        previousLabel={'< Previous'}
        forcePage={selectedPage}
        renderOnZeroPageCount={null}
      />
      <Dialog open={!isNull(listingToDelete)} onClose={() => setListingToDelete(null)}>
        <DialogContent>
          <DialogContentText>Are you sure you want to delete this listing?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={() => setListingToDelete(null)}>
            Cancel
          </Button>
          <Button color={'error'} onClick={() => deleteListing(listingToDelete)}>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

CurrentListingsPage.displayName = 'CurrentListingsPage';
