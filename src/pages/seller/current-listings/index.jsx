import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';

const CurrentListingsPage = loadable(() => import(/* webpackChunkName: "seller-page" */ './page'), {
  resolveComponent: ({ CurrentListingsPage: Page }) => Page,
  fallback: <Loading />,
});

export default CurrentListingsPage;
