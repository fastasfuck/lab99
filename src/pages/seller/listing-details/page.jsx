import { getContractPreviewThunk, getListingByIdThunk } from 'features/common/thunks/listing';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Carousel from 'react-gallery-carousel';
import { checkIfIsLoading } from 'features/common/slices/api/index';
import Loading from 'features/common/components/Loading';
import { Typography, Grid, Dialog, DialogContent, DialogContentText } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import styles from './styles.module.css';
import { selectCurrentUserRole } from 'features/common/slices/auth';
import classNames from 'classnames/bind';
import { getFullAddress } from 'src/utils/addressFormatter';
import ROLES from 'features/common/enums';

const cx = classNames.bind(styles);

/* eslint-disable import/prefer-default-export */
export function ListingDetailsPage() {
  const [listing, setListing] = useState(null);
  const [contract, setContract] = useState(null);
  const isLoading = useSelector(checkIfIsLoading);
  const role = useSelector(selectCurrentUserRole);
  const dispatch = useDispatch();
  const params = useParams();

  useEffect(async () => {
    const response = await dispatch(getListingByIdThunk(params.listingId));
    if (response.payload.success) {
      setListing(response.payload.data);
    }
  }, []);

  const generateContract = async () => {
    const response = await dispatch(getContractPreviewThunk({ listingId: listing.id }));
    if (response.payload.success){
      setContract(response.payload.data);
    }
  }

  return (
    <div>
      {isLoading && <Loading />}
      {listing && (
        <div>
          {listing.photos && listing.photos.length > 0 && (
            <Carousel
              images={listing.photos.map((photo) => ({ src: photo.url }))}
              style={{ height: 600, width: '100%' }}
            />
          )}
          <Typography className={cx('info-item')} variant="body1">
            {listing.property_information || 'No description'}
          </Typography>
          <Typography className={cx('main-subtitle')} component="p" variant="subtitle">
            Address
          </Typography>
          <Typography className={cx('info-item')} variant="body1">
            {getFullAddress(listing.address)}
          </Typography>
          <Typography variant="subtitle" component="p" className={cx('main-subtitle')}>
            Property information
          </Typography>
          <Grid className={cx('info-block')} container spacing={'8px'}>
            <Grid item xs={3}>
              <Typography variant="subtitle">Property Size</Typography>
              <Typography className={cx('info-item')} variant="body1">
                {listing.property_size} Sq
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Garden Size</Typography>
              <Typography className={cx('info-item')} variant="body1">
                {listing.garden_size} Sq
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Terrace Size</Typography>
              <Typography className={cx('info-item')} variant="body1">
                {listing.terrace_size} Sq
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Bedrooms</Typography>
              <Typography className={cx('info-item')} variant="body1">
                {listing.bedrooms}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Bathrooms</Typography>
              <Typography className={cx('info-item')} variant="body1">
                {listing.bathrooms}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Garages</Typography>
              <Typography className={cx('info-item')} variant="body1">
                {listing.garages}
              </Typography>
            </Grid>
          </Grid>
          <Typography variant="subtitle" component="p" className={cx('main-subtitle')}>
            Prices information
          </Typography>
          <Grid className={cx('info-block')} container spacing={'8px'}>
            <Grid item xs={3}>
              <Typography variant="subtitle">Cost basis</Typography>
              <Typography className={cx('info-item')} variant="body1">
                ${listing.cost}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Market price</Typography>
              <Typography className={cx('info-item')} variant="body1">
                ${listing.market_price}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Price</Typography>
              <Typography className={cx('info-item')} variant="body1">
                ${listing.current_price}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Seller asked</Typography>
              <Typography className={cx('info-item')} variant="body1">
                ${listing.asking_price}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Bid asked</Typography>
              <Typography className={cx('info-item')} variant="body1">
                ${listing.option_price}
              </Typography>
            </Grid>
          </Grid>
          <Grid className={cx('info-block')} container spacing={'8px'}>
            <Grid item xs={3}>
              <Typography variant="subtitle">Bidding ends</Typography>
              <Typography className={cx('info-item')} variant="body1">
                {listing.bidding_ends}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Year built</Typography>
              <Typography className={cx('info-item')} variant="body1">
                {listing.year_built}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle">Postal code</Typography>
              <Typography className={cx('info-item')} variant="body1">
                {listing.postal_code}
              </Typography>
            </Grid>
          </Grid>
          {role === ROLES.Seller && <LoadingButton loading={isLoading} type="button" variant="secondary" onClick={generateContract}>Show contract</LoadingButton>}
        </div>
      )}
            <Dialog fullWidth open={!!contract} onClose={() => setContract(null)}>
        <DialogContent>
          <DialogContentText>
            <p dangerouslySetInnerHTML={{ __html: contract }}/>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}

ListingDetailsPage.displayName = 'ListingDetailsPage';
