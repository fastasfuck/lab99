import { Typography } from '@mui/material';
import { Helmet } from 'react-helmet';
import DashboardContainer from 'features/seller/containers/Dashboard';

/* eslint-disable import/prefer-default-export */
export function SellerHomePage() {
  return (
    <>
      <Helmet>
        <title>Alphamize | Dashboard</title>
      </Helmet>
      <Typography variant="h5" gutterBottom>
        Dashboard
      </Typography>
      <DashboardContainer />
    </>
  );
}

SellerHomePage.displayName = 'SellerHomePage';
