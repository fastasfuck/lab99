import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';

const ProfilePage = loadable(() => import(/* webpackChunkName: "profile-page" */ './page'), {
  resolveComponent: ({ ProfilePage: Page }) => Page,
  fallback: <Loading />,
});

export default ProfilePage;
